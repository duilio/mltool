mltool: regression tool
=======================

mltool is a simple `Learning to Rank`_ tool to build a regression tree based ranking model.
Currently mltool supports simple CART and `Random Forest`_. The implementation is strongly
inspired by `rt-rank`_, but it lacks the support of `Gradient Boosting Regression Trees`_.

.. _`Learning to Rank`: http://en.wikipedia.org/wiki/Learning_to_rank
.. _`Random Forest`: http://stat-www.berkeley.edu/users/breiman/RandomForests/cc_home.htm
.. _`rt-rank`: https://sites.google.com/site/rtranking/
.. _`Gradient Boosting Regression Trees`: http://en.wikipedia.org/wiki/Gradient_boosting


Overview
--------

Features
~~~~~~~~

Despite rt-rank, mltool provides:

- A parameter to set the seed for the random number generator to make the training
  deterministic
- Serializable model
- Show feature information gain statistics at the end of the training
- Can be used as an API

Some highlights compared to other Random Forest implemented for Python:

- The implementation of the Random Forest makes use of numpy and it is quite optimized
- Parallel Random Forest training


Installation
~~~~~~~~~~~~

Install from PyPI using pip::

   $ pip install mltool

Now you can run mltool::

   $ mltool -h

Documentation
~~~~~~~~~~~~~

The documentation is hosted by `Read the Docs`_ at the following url:
http://readthedocs.org/docs/mltool/en/latest/

.. _`Read the Docs`: http://readthedocs.org/

Future
~~~~~~

- add support for Stochastic Gradient Boosting Regression Trees
- add support for simple regression and/or classification (i.e. not just focus on ranking)
