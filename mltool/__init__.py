"""
mltool
======

:copyright: (c) 2012 by Maurizio Sambati
:license: MIT, see LICENSE for more details.

"""
__version__ = '0.5.1b'
__author__ = 'Maurizio Sambati'
__license__ = 'MIT'
__copyright__ = 'Copyright 2012 Maurizio Sambati'
