.. mltool documentation master file, created by
   sphinx-quickstart on Wed Apr  4 08:26:52 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../README

User Guide
----------

mltool can be used as a command line tools.
This part of the documentation covers each command supported by mltool.

.. toctree::
   :maxdepth: 2

   user

API Documentation
-----------------

mltool can be used as a library for prediction or for building your own
custom models.

.. toctree::
   :maxdepth: 2

   api
