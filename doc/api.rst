.. _api:

API Documentation
=================

.. _`embed model`:

Embed the model
---------------

One of the most common use of the mltool API is to embed the ranker model built
with the command line tool into your own application.

The models are pickable file and you can load them using the :mod:`pickle` standard
module:

.. code-block:: python

    import pickle
    with open('model.pkl', 'rb') as fmodel:
        model = pickle.load(fmodel)

Then you can use mltool API to predict the score of a sample:

.. code-block:: python

    from mltool.predict import predict
    pred = predict(model, {'f0': 1.0,
                           'f1': 0.0,
                           'f2': 0.0,
                           'f3': 0.2,
                           'f4': 1.0})

Check :func:`predict <mltool.predict.predict>` and
:func:`predict_all <mltool.predict.predict_all>` functions for more details.

.. module:: mltool

Prediction
----------

.. _predict:

.. automodule:: mltool.predict

-----------------

.. autofunction:: predict

-----------------

.. autofunction:: predict_all

.. _train:

Model train
-----------

mltool implements some algorithms to train regression models.
Currently it mainly supports Random Forest and regression trees.

mltool.forest
~~~~~~~~~~~~~

.. module:: mltool.forest
.. autofunction:: train_random_forest

mltool.decisiontree
~~~~~~~~~~~~~~~~~~~

.. module:: mltool.decisiontree
.. autofunction:: train_decision_tree


Model Evaluation
----------------

.. _evaluate:

.. automodule:: mltool.evaluate

-----------------

.. autofunction:: evaluate_preds
.. autofunction:: evaluate_model


.. _utilities:

Utilities
---------

.. module:: mltool.utils

.. _dataset:

Handling Dataset
~~~~~~~~~~~~~~~~

.. class:: Dataset

    The Dataset class is a :func:`namedtuple <collections.namedtuple>` which
    represents a set of samples with their labels and query ids.

    .. attribute:: labels

        An :class:`array <numpy.array>` of labels. Each label is a `float`.

    .. attribute:: queries

        A list of query ids.

    .. attribute:: Dataset.samples

        A 2d-:class:`array <numpy.array` of samples. It consists of one sample per
        column, and one row for each feature.

    .. attribute:: Dataset.feature_names

        A sequence of feature names. Features are in the same order as they appear
        in the :attr:`samples` rows.

-----------------

.. autofunction:: read_input_file

    
